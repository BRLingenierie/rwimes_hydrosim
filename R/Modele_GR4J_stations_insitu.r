# use roxygen2:
# https://cran.r-project.org/web/packages/roxygen2/vignettes/rd.html
# run roxygen2::roxygenise() to generate documentation

# Nouvelle version de wimes pas de url et de key
testPurpose = function() {
  
  url="https://wimes-hydrosim.brl.fr/" 
  key="5b805acc-db24-4740-b221-87bd1cdffeff"
    
  # library pour le modele GR4J
  library(airGR)
  # library qui permet de traiter les donnees 
  library(zoo)
  # pour r?cup?rer les donnees au barrage 
  library(rwimes)
  library(devtools)
  # pour extrapoler les courbes H/V au dela de leur zone 
  library(rgdal)
  library(raster)
  library(geojson)
  library(geojsonR)
  wimesInitialization(url, key)
  
}


modelisation_gr_journalier_smartbasin_debit_entrant_TRMM <- function(cd) {
  
  ############################ 1 - Variables  #####################################################################
  logInfo("1 - Variables")
  
  PARAM_BV_STATION = rwimeshydrosim::Info_bv_Pluie_KED_ETP_CRU2018_GR5J_HU_DEBRIDE
  
  CODE_HYDRO = PARAM_BV_STATION$Station[cd] 
  
  PARAMETRE_GR = PARAM_BV_STATION[cd,6:10]
  
  SUPERFICIE = PARAM_BV_STATION[cd,2]
  
  # Returns the geometry of the site as a GDAL polygon
  logInfo("--- Getting the geometry of the site as a GDAL polygon")
  
  bv = GetGeometry(  as.character(CODE_HYDRO))

  MAX_RETOUR_AR = 360*24 # pb d?calage obs/simu de ~3jours
  
  ######################## 2- on regarde les dates sur lesquelles on travaille ##
  logInfo("2 - On regarde les dates sur lesquelles on travaille")
  
  # date a laquelle on lance la simulation
  date_obs = paste0(substr(format(Sys.time(), 
                                  "%Y-%m-%d %H:%M:%S"), 1, 13), ":00:00")
  
  # Mise en date du format des observations
  DATE_H_MODEL_OBS = strptime(date_obs,
                              format = "%Y-%m-%d %H:%M:%S")
  
  # FORMAT AU PAS DE 1H - on regarde 3 j avant 
  DATE_H_MODEL = seq(DATE_H_MODEL_OBS -  MAX_RETOUR_AR * 3600, 
                     DATE_H_MODEL_OBS, by = "day")
  

  
  ######################## 4- Extraire les pluies TRMMS de BASSIN  ################################################
  logInfo("4 - Extraire les pluies TRMM de BASSIN")

  PLUIE_TRMM_NOM = getObservedValues(
    "GPM_GRID_P_1D",
    beginDateTime = DATE_H_MODEL[1],
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]
  )
 
  PLUIE_TRMM_PAS_TEMPS_FIXE = rep(0,length(DATE_H_MODEL))
   
  for ( i in 1:length( DATE_H_MODEL) )
  {
   
        M_DATE = which( paste0( substr( DATE_H_MODEL[i] ,1 ,10 )," 23:30:00")  == substr(PLUIE_TRMM_NOM$observationDateTime ,1 ,19) )
        
        # condition pour avoir les 8 pas de temps
        if(length(M_DATE) ==  1)
        {
          
          gridFile = getGrid(PLUIE_TRMM_NOM$value[M_DATE])
          #print(paste0("download grid ", PLUIE_TRMM_NOM$value[M_DATE]))
          rainfall_raster = raster(gridFile)
          
                    # logInfo("Rasterizing the basin...")
          raster_bv = rasterize(bv, rainfall_raster, getCover = TRUE, silent=TRUE)
          
          # logInfo("Calculating basin rainfall")
          sum_raster_bv = cellStats(raster_bv, sum)
          if (!is.na(sum_raster_bv) & sum_raster_bv > 0) {
            PLUIE_TRMM_PAS_TEMPS_FIXE [i] = cellStats(raster_bv * rainfall_raster, sum) / sum_raster_bv
          }  
          print(paste0("Trying to remove file ", gridFile))
          result = file.remove(gridFile)
          if (result) {
            print("File deleted")
          } else {
            print("File not deleted")
          }
        }
  }

  
  # dans le cas ou nous n avons pas de pluie meteo france
  if(is.null( PLUIE_TRMM_NOM)) {
    # TODO JWI
    logError("No TRMM rainfall")
    stop("No TRMM rainfall")
    
  }      
  
  
  ######################## 6- Extraire les ETP  ################################################
  logInfo("6 - Extraire les ETP")
  
  DATE_ETP_JOURS = paste0(substr(DATE_H_MODEL, 6, 7), "-",substr(DATE_H_MODEL, 9, 10) )
  ETP_DATE_HOURS = rep(0, length(DATE_ETP_JOURS))
  
  CRU_PET = rwimesuganda::CRU_ETP_82217 
    
  date  = strptime(CRU_PET[,1] ,"%Y-%m-%d")
  
  PET_daily = aggregate( zoo(CRU_PET[,2],order.by = date) , format(date,"%m-%d") ,mean)
  
  # On determine la date exacte et l ETP /24
  for (i in seq(along = ETP_DATE_HOURS)) {
    ETP_DATE_HOURS[i] = PET_daily[which(time(PET_daily) == DATE_ETP_JOURS[i])]
  }     
  
  
  
  ############################### 9 - integre les etats initiaux #################################################################################

  logInfo("9 - Integration des etats initiaux")
  
  
  DATES_VARIABLES_INITIALISATION  =    DATE_H_MODEL 
 
  
  VARIABLES_INITIALISATION = array(NA,dim=c(length(DATES_VARIABLES_INITIALISATION)
                                            ,10))
  
  VARIABLES_INITIALISATION = data.frame( VARIABLES_INITIALISATION)
  names( VARIABLES_INITIALISATION) = c( "S0" , "R0" ,"Q0" ,"Date")
  
  
  VARIABLES_INITIALISATION$Date = DATES_VARIABLES_INITIALISATION
  
  
  S0 = getObservedValues(
    paste0("BRL_GR5J_",CODE_HYDRO,"_S0"), 
    beginDateTime = DATE_H_MODEL[1] ,
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  if(is.null(S0$value)!= TRUE) {
    VARIABLES_INITIALISATION$S0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
                                                                   S0)$VARIABLE_TEMP  
  }         
  
  R0 = getObservedValues(
    paste0("BRL_GR5J_",CODE_HYDRO,"_R0"),
    beginDateTime = DATE_H_MODEL[1] ,
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  if(is.null(R0$value)!= TRUE) {
    VARIABLES_INITIALISATION$R0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
                                                                     R0)$VARIABLE_TEMP  
  }
  
  
  # d?bit observ? en ouganda  
  #  Q0 = getObservedValues(
  #     paste0(CODE_HYDRO,"_Qec"), 
  # #   beginDateTime = DATE_H_MODEL[1] ,
  #  endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  
  #if(is.null(Q0$value)!= TRUE) {
  #  VARIABLES_INITIALISATION$Q0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
  #                                                                   Q0)$VARIABLE_TEMP  
  #}
  
  

  #########################################
  
  S0 = VARIABLES_INITIALISATION$S0[1]
  
  
  R0 = VARIABLES_INITIALISATION$R0[1]
  
  
  # Q0 = VARIABLES_INITIALISATION$Q0[1]
  Q0 = NA 
  
  # Dans le cas ou les etats initiaux ne sont pas trouves on les impose a ces valeurs  
  if (is.na(S0[[1]])) {
    
    S0 = 0.5
    
    logInfo("Condition initiale degrade du reservoir de production de GR4J ")
    
  } else {
    
    S0 = as.numeric(S0)
    
  } 
  
  # Deux cas pour R0 - calcul du debit initial
  if (is.na(Q0[[1]])) {
    
    # si pas d information sur le debit on prend l etat initial 
    # par defaut la modelisation avant sinon a 0.3)  
    if (is.na(R0[[1]])) { 
      
      R0 = 0.3 
      logInfo("Condition initiale degrade du reservoir de transfert de GR4J ")
      
    } else { 
      
      R0 = as.numeric(R0)
    
      }  
  
    } else {
    
    R0 = as.numeric(OPTIMISATION_RESERVOIR_TRANFERT(parametres_station = parametres_station,
                                                    DATE_H_MODEL = DATE_H_MODEL,
                                                    PLUIE_DATE_HOURS = PLUIE_TRMM_PAS_TEMPS_FIXE , 
                                                    ETP_DATE_HOUR = ETP_DATE_HOURS , 
                                                    S0 = S0,
                                                    Q0 = Q0,
                                                    DATE_RECALAGE = VARIABLES_INITIALISATION$Date[1]))
     } # fin du else sur la condition R0
  

  ############################### 10- Lancer la modelisation GR ####################################################################################
  logInfo("10 - Lancer la modelisation GR")
  
  # preparation of InputsModel object
  InputsModel <- CreateInputsModel(
    FUN_MOD = RunModel_GR5J,
    DatesR = DATE_H_MODEL,
    Precip = PLUIE_TRMM_PAS_TEMPS_FIXE,
    PotEvap = ETP_DATE_HOURS)
  
  ## On met en route sur l ensemble de la periode souhaitee
  Ind_Run <- seq(
    which(format(DATE_H_MODEL, 
                 format = "%d/%m/%Y %H:%M") == format(DATE_H_MODEL[1], 
                                                      format = "%d/%m/%Y %H:%M")),
    which(format(DATE_H_MODEL, 
                 format = "%d/%m/%Y %H:%M") == format(DATE_H_MODEL[length(DATE_H_MODEL)],
                                                      format = "%d/%m/%Y %H:%M")))
  
  ## Preparation de RunOptions : object, periode de chauffe -> pas d impact 
  RunOptions <- CreateRunOptions(
    FUN_MOD = RunModel_GR5J,
    InputsModel = InputsModel,
    IndPeriod_Run = Ind_Run,
    IndPeriod_WarmUp = 1:(Ind_Run[1]-1)) 
  
  # nouvelle version du modele GR4J   
  IniStates <- CreateIniStates(FUN_MOD = RunModel_GR5J, InputsModel = InputsModel,
                               ProdStore = S0*PARAMETRE_GR$X1,
                               RoutStore = R0*PARAMETRE_GR$X3, 
                               ExpStore = NULL,
                               UH1 = NULL,
                               UH2 = NULL,
                               GCemaNeigeLayers = NULL, eTGCemaNeigeLayers = NULL,
                               GthrCemaNeigeLayers = NULL, GlocmaxCemaNeigeLayers = NULL)
  
  ## Preparation de RunOptions : object, periode de chauffe -> pas d impact 
  RunOptions <- CreateRunOptions(FUN_MOD = RunModel_GR5J, 
                                 InputsModel = InputsModel, 
                                 IndPeriod_Run = Ind_Run, IndPeriod_WarmUp = 1:(Ind_Run[1]-1),
                                 IniStates = IniStates)
  
  
  OutputsModel = tryCatch(
    
    myFun_GR(InputsModel,RunOptions,PARAMETRE_GR$X1,PARAMETRE_GR$X2,
             PARAMETRE_GR$X3,PARAMETRE_GR$X4,PARAMETRE_GR$X5, RunModel_GR5J),
    
    error = function(e) {
      
      # nouvelle version du modele GR4J   
      IniStates <- CreateIniStates(FUN_MOD = RunModel_GR5J, InputsModel = InputsModel,
                                   ProdStore = S0*PARAMETRE_GR$X1,
                                   RoutStore = 0.3*PARAMETRE_GR$X3, 
                                   ExpStore = NULL,
                                   UH1 = NULL,
                                   UH2 = NULL,
                                   GCemaNeigeLayers = NULL, eTGCemaNeigeLayers = NULL,
                                   GthrCemaNeigeLayers = NULL, GlocmaxCemaNeigeLayers = NULL)
      
      # Preparation de RunOptions : object, periode de chauffe -> pas d impact 
      RunOptions <- CreateRunOptions(FUN_MOD = RunModel_GR5J, 
                                     InputsModel = InputsModel, 
                                     IndPeriod_Run = Ind_Run, 
                                     IndPeriod_WarmUp = 1:(Ind_Run[1]-1),
                                     IniStates = IniStates)
      
      return(OutputsModel = RunModel(InputsModel = InputsModel, 
                                     RunOptions = RunOptions,
                                     Param = c(PARAMETRE_GR$X1, 
                                               PARAMETRE_GR$X2, 
                                               PARAMETRE_GR$X3, 
                                               PARAMETRE_GR$X4,
                                               PARAMETRE_GR$X5), 
                                     FUN = RunModel_GR5J))
      logInfo("Pas d'optimisation des niveaux initiaux -> S0 = S0 mais R0 = 0.3") 
    }
  )
  
  
  # On recupere la lame d eau de la modelisation + conversion des mm/h en m3/s
  DEBIT_ENTREE_2 = OutputsModel$Qsim *SUPERFICIE / (24*3.6)
  
  # on recupere les niveaux d eau dans les reservoirs de production et de transfert
  
  S_OUT = data.frame(DATE_H_MODEL, OutputsModel$Prod /PARAMETRE_GR$X1)
  
  R_OUT = data.frame(DATE_H_MODEL, OutputsModel$Rout /PARAMETRE_GR$X3)
  
  ################################ 11 - on va chercher les donnees 
  #hydrometriques sur la fenetre d analyse  ############################
  #logInfo("11 - On va chercher les donnees hydrometriques sur la fenetre d'analyse")
  
  #DEBIT_STATION = getObservedValues(
  #   paste0(CODE_HYDRO,"_Qec"),
  #  beginDateTime = as.POSIXct(DATE_H_MODEL[1], format =  "%Y-%m-%d %H:%M:%S"),
  #  endDateTime = as.POSIXct(DATE_H_MODEL[length(DATE_H_MODEL)], format =  "%Y-%m-%d %H:%M:%S")
  #)$value
  
  #DEBIT_STATION_DATE = getObservedValues(
  #  paste0(CODE_HYDRO,"_Qec"),
  #  beginDateTime = as.POSIXct(DATE_H_MODEL[1], format = "%Y-%m-%d %H:%M:%S"),
  #  endDateTime = as.POSIXct(DATE_H_MODEL[length(DATE_H_MODEL)], format = "%Y-%m-%d %H:%M:%S")
  #)$observationDateTime
  
  # on met au pas de temps horaire fixe les debits a la station : Apremont, vouraie, mervent, rochereau
  # DEBIT_STATION_PAS_TEMPS_FIXE = rep(NA, length(DATE_H_MODEL)) 
  
  #for (i in 1:length(DATE_H_MODEL)) {
  #  
  #  m_date = which(substr(DATE_H_MODEL[i],1,16) == substr(DEBIT_STATION_DATE,1,16))
  #  
  #  if (length(m_date) != 0) DEBIT_STATION_PAS_TEMPS_FIXE[i] = DEBIT_STATION[m_date]
    
  #}
  

  ################################ 12 - Ecriture des resultats  ##############################################################
  
  logInfo("12 - Ecriture des resultats")
  
  # On ecrit dans WIMES les debits modelises 
  date_prevision_Q = format(Sys.time(),"%Y-%m-%d %H:%M:%S" ) 
  
  ############## on met les na en null 
  

  # on sauvegarde les niveaux dans les reservoirs de transfert   
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_",CODE_HYDRO,"_R0"), 
      observationDateTime = DATE_H_MODEL,
      value = R_OUT[,2]
    )
  )
  
  # on sauvegarde les niveaux dans les reservoirs de transfert   
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_",CODE_HYDRO,"_S0"), 
      observationDateTime = DATE_H_MODEL,
      value = S_OUT[,2]
    )
  )
  
  
  # on sauvegarde les debits issus du modele hydrologique
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_",CODE_HYDRO,"_Q"),
      observationDateTime =  DATE_H_MODEL,
      value = DEBIT_ENTREE_2
    )
  )   
  
}

modelisation_gr_journalier_smartbasin_debit_entrant_TRMM_ST <- function(cd) {
  
  ############################ 1 - Variables  #####################################################################
  logInfo("1 - Variables")
  
  PARAM_BV_STATION = rwimeshydrosim::Info_bv_ST_Pluie_KED_ETP_CRU2018_GR5J_HU_DEBRIDE
  
  CODE_HYDRO = PARAM_BV_STATION$Code[cd] 
  
  PARAMETRE_GR = PARAM_BV_STATION[cd,4:7]
  
  SUPERFICIE = PARAM_BV_STATION[cd,2]
  
  # Returns the geometry of the site as a GDAL polygon
  logInfo("--- Getting the geometry of the site as a GDAL polygon")
  bv = GetGeometry(  as.character(CODE_HYDRO))
  
  MAX_RETOUR_AR = 360*24 # pb d?calage obs/simu de ~3jours
  
  ######################## 2- on regarde les dates sur lesquelles on travaille ##
  logInfo("2 - On regarde les dates sur lesquelles on travaille")
  
  # date a laquelle on lance la simulation
  date_obs = paste0(substr(format(Sys.time(), 
                                  "%Y-%m-%d %H:%M:%S"), 1, 13), ":00:00")
  
  # Mise en date du format des observations
  DATE_H_MODEL_OBS = strptime(date_obs,
                              format = "%Y-%m-%d %H:%M:%S")
  
  # FORMAT AU PAS DE 1H - on regarde 3 j avant 
  DATE_H_MODEL = seq(DATE_H_MODEL_OBS -  MAX_RETOUR_AR * 3600, 
                     DATE_H_MODEL_OBS, by = "day")
  
  
  
  ######################## 4- Extraire les pluies TRMMS de BASSIN  ################################################
  logInfo("4 - Extraire les pluies TRMM de BASSIN")
  
  PLUIE_TRMM_NOM = getObservedValues(
    "GPM_GRID_P_1D",
    beginDateTime = DATE_H_MODEL[1],
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]
  )
  
  PLUIE_TRMM_PAS_TEMPS_FIXE = rep(0,length(DATE_H_MODEL))
  
  for ( i in 1:length( DATE_H_MODEL) )
  {
    
    M_DATE = which( paste0( substr( DATE_H_MODEL[i] ,1 ,10 )," 23:30:00")  == substr(PLUIE_TRMM_NOM$observationDateTime ,1 ,19) )
    
    # condition pour avoir les 8 pas de temps
    if(length(M_DATE) ==  1)
    {
      gridFile = getGrid(PLUIE_TRMM_NOM$value[M_DATE])
      rainfall_raster = raster(gridFile)
      
      # logInfo("Rasterizing the basin...")
      raster_bv = rasterize(bv, rainfall_raster, getCover = TRUE, silent=TRUE)
      
      # logInfo("Calculating basin rainfall")
      sum_raster_bv = cellStats(raster_bv, sum)
      if (!is.na(sum_raster_bv) & sum_raster_bv > 0) {
        PLUIE_TRMM_PAS_TEMPS_FIXE [i] = cellStats(raster_bv * rainfall_raster, sum) / sum_raster_bv
      }
      
      print(paste0("Trying to remove file ", gridFile))
      result = file.remove(gridFile)
      if (result) {
        print("File deleted")
      } else {
        print("File not deleted")
      }
    }
  }
  
  
  # dans le cas ou nous n avons pas de pluie meteo france
  if(is.null( PLUIE_TRMM_NOM)) {
    # TODO JWI
    logError("No TRMM rainfall")
    stop("No TRMM rainfall")
    
  }      
  
  
  ######################## 6- Extraire les ETP  ################################################
  logInfo("6 - Extraire les ETP")
  
  DATE_ETP_JOURS = paste0(substr(DATE_H_MODEL, 6, 7), "-",substr(DATE_H_MODEL, 9, 10) )
  ETP_DATE_HOURS = rep(0, length(DATE_ETP_JOURS))
  
  CRU_PET = rwimesuganda::CRU_ETP_82217 
  
  date  = strptime(CRU_PET[,1] ,"%Y-%m-%d")
  
  PET_daily = aggregate( zoo(CRU_PET[,2],order.by = date) , format(date,"%m-%d") ,mean)
  
  # On determine la date exacte et l ETP /24
  for (i in seq(along = ETP_DATE_HOURS)) {
    ETP_DATE_HOURS[i] = PET_daily[which(time(PET_daily) == DATE_ETP_JOURS[i])]
  }     
  
  
  
  ############################### 9 - integre les etats initiaux #################################################################################
  
  logInfo("9 - Integration des etats initiaux")
  
  
  DATES_VARIABLES_INITIALISATION  =    DATE_H_MODEL 
  
  
  VARIABLES_INITIALISATION = array(NA,dim=c(length(DATES_VARIABLES_INITIALISATION)
                                            ,10))
  
  VARIABLES_INITIALISATION = data.frame( VARIABLES_INITIALISATION)
  names( VARIABLES_INITIALISATION) = c( "S0" , "R0" ,"Q0" ,"Date")
  
  
  VARIABLES_INITIALISATION$Date = DATES_VARIABLES_INITIALISATION
  
  
  S0 = getObservedValues(
    paste0("BRL_GR5J_RANDOM_FOREST_",CODE_HYDRO,"_S0"), 
    beginDateTime = DATE_H_MODEL[1] ,
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  if(is.null(S0$value)!= TRUE) {
    VARIABLES_INITIALISATION$S0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
                                                                     S0)$VARIABLE_TEMP  
  }         
  
  R0 = getObservedValues(
    paste0("BRL_GR5J_RANDOM_FOREST_",CODE_HYDRO,"_R0"),
    beginDateTime = DATE_H_MODEL[1] ,
    endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  if(is.null(R0$value)!= TRUE) {
    VARIABLES_INITIALISATION$R0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
                                                                     R0)$VARIABLE_TEMP  
  }
  
  
  # d?bit observ? en ouganda  
  #  Q0 = getObservedValues(
  #     paste0(CODE_HYDRO,"_Qec"), 
  # #   beginDateTime = DATE_H_MODEL[1] ,
  #  endDateTime = DATE_H_MODEL[length(DATE_H_MODEL)]) 
  
  
  #if(is.null(Q0$value)!= TRUE) {
  #  VARIABLES_INITIALISATION$Q0 = INITIALISATION_VARIABLES_PDT_FIXE (DATES_VARIABLES_INITIALISATION,
  #                                                                   Q0)$VARIABLE_TEMP  
  #}
  
  
  
  #########################################
  
  S0 = VARIABLES_INITIALISATION$S0[1]
  
  
  R0 = VARIABLES_INITIALISATION$R0[1]
  
  
  # Q0 = VARIABLES_INITIALISATION$Q0[1]
  Q0 = NA 
  
  # Dans le cas ou les etats initiaux ne sont pas trouves on les impose a ces valeurs  
  if (is.na(S0[[1]])) {
    
    S0 = 0.5
    
    logInfo("Condition initiale degrade du reservoir de production de GR4J ")
    
  } else {
    
    S0 = as.numeric(S0)
    
  } 
  
  # Deux cas pour R0 - calcul du debit initial
  if (is.na(Q0[[1]])) {
    
    # si pas d information sur le debit on prend l etat initial 
    # par defaut la modelisation avant sinon a 0.3)  
    if (is.na(R0[[1]])) { 
      
      R0 = 0.3 
      logInfo("Condition initiale degrade du reservoir de transfert de GR4J ")
      
    } else { 
      
      R0 = as.numeric(R0)
      
    }  
    
  } else {
    
    R0 = as.numeric(OPTIMISATION_RESERVOIR_TRANFERT(parametres_station = parametres_station,
                                                    DATE_H_MODEL = DATE_H_MODEL,
                                                    PLUIE_DATE_HOURS = PLUIE_TRMM_PAS_TEMPS_FIXE , 
                                                    ETP_DATE_HOUR = ETP_DATE_HOURS , 
                                                    S0 = S0,
                                                    Q0 = Q0,
                                                    DATE_RECALAGE = VARIABLES_INITIALISATION$Date[1]))
  } # fin du else sur la condition R0
  
  
  ############################### 10- Lancer la modelisation GR ####################################################################################
  logInfo("10 - Lancer la modelisation GR")
  
  # preparation of InputsModel object
  InputsModel <- CreateInputsModel(
    FUN_MOD = RunModel_GR4J,
    DatesR = DATE_H_MODEL,
    Precip = PLUIE_TRMM_PAS_TEMPS_FIXE,
    PotEvap = ETP_DATE_HOURS)
  
  ## On met en route sur l ensemble de la periode souhaitee
  Ind_Run <- seq(
    which(format(DATE_H_MODEL, 
                 format = "%d/%m/%Y %H:%M") == format(DATE_H_MODEL[1], 
                                                      format = "%d/%m/%Y %H:%M")),
    which(format(DATE_H_MODEL, 
                 format = "%d/%m/%Y %H:%M") == format(DATE_H_MODEL[length(DATE_H_MODEL)],
                                                      format = "%d/%m/%Y %H:%M")))
  
  ## Preparation de RunOptions : object, periode de chauffe -> pas d impact 
  RunOptions <- CreateRunOptions(
    FUN_MOD = RunModel_GR4J,
    InputsModel = InputsModel,
    IndPeriod_Run = Ind_Run,
    IndPeriod_WarmUp = 1:(Ind_Run[1]-1)) 
  
  # nouvelle version du modele GR4J   
  IniStates <- CreateIniStates(FUN_MOD = RunModel_GR4J, InputsModel = InputsModel,
                               ProdStore = S0*PARAMETRE_GR$X1,
                               RoutStore = R0*PARAMETRE_GR$X3, 
                               ExpStore = NULL,
                               UH1 = NULL,
                               UH2 = NULL,
                               GCemaNeigeLayers = NULL, eTGCemaNeigeLayers = NULL,
                               GthrCemaNeigeLayers = NULL, GlocmaxCemaNeigeLayers = NULL)
  
  ## Preparation de RunOptions : object, periode de chauffe -> pas d impact 
  RunOptions <- CreateRunOptions(FUN_MOD = RunModel_GR4J, 
                                 InputsModel = InputsModel, 
                                 IndPeriod_Run = Ind_Run, IndPeriod_WarmUp = 1:(Ind_Run[1]-1),
                                 IniStates = IniStates)
  
  
  OutputsModel = tryCatch(
    
    myFun_GR(InputsModel,RunOptions,PARAMETRE_GR$X1,PARAMETRE_GR$X2,
             PARAMETRE_GR$X3,PARAMETRE_GR$X4, RunModel_GR4J),
    
    error = function(e) {
      
      # nouvelle version du modele GR4J   
      IniStates <- CreateIniStates(FUN_MOD = RunModel_GR4J, InputsModel = InputsModel,
                                   ProdStore = S0*PARAMETRE_GR$X1,
                                   RoutStore = 0.3*PARAMETRE_GR$X3, 
                                   ExpStore = NULL,
                                   UH1 = NULL,
                                   UH2 = NULL,
                                   GCemaNeigeLayers = NULL, eTGCemaNeigeLayers = NULL,
                                   GthrCemaNeigeLayers = NULL, GlocmaxCemaNeigeLayers = NULL)
      
      # Preparation de RunOptions : object, periode de chauffe -> pas d impact 
      RunOptions <- CreateRunOptions(FUN_MOD = RunModel_GR4J, 
                                     InputsModel = InputsModel, 
                                     IndPeriod_Run = Ind_Run, 
                                     IndPeriod_WarmUp = 1:(Ind_Run[1]-1),
                                     IniStates = IniStates)
      
      return(OutputsModel = RunModel(InputsModel = InputsModel, 
                                     RunOptions = RunOptions,
                                     Param = c(PARAMETRE_GR$X1, 
                                               PARAMETRE_GR$X2, 
                                               PARAMETRE_GR$X3, 
                                               PARAMETRE_GR$X4), 
                                     FUN = RunModel_GR4J))
      logInfo("Pas d'optimisation des niveaux initiaux -> S0 = S0 mais R0 = 0.3") 
    }
  )
  
  
  # On recupere la lame d eau de la modelisation + conversion des mm/h en m3/s
  DEBIT_ENTREE_2 = OutputsModel$Qsim *SUPERFICIE / (24*3.6)
  
  # on recupere les niveaux d eau dans les reservoirs de production et de transfert
  
  S_OUT = data.frame(DATE_H_MODEL, OutputsModel$Prod /PARAMETRE_GR$X1)
  
  R_OUT = data.frame(DATE_H_MODEL, OutputsModel$Rout /PARAMETRE_GR$X3)
  
  ################################ 11 - on va chercher les donnees 
  #hydrometriques sur la fenetre d analyse  ############################
  #logInfo("11 - On va chercher les donnees hydrometriques sur la fenetre d'analyse")
  
  #DEBIT_STATION = getObservedValues(
  #   paste0(CODE_HYDRO,"_Qec"),
  #  beginDateTime = as.POSIXct(DATE_H_MODEL[1], format =  "%Y-%m-%d %H:%M:%S"),
  #  endDateTime = as.POSIXct(DATE_H_MODEL[length(DATE_H_MODEL)], format =  "%Y-%m-%d %H:%M:%S")
  #)$value
  
  #DEBIT_STATION_DATE = getObservedValues(
  #  paste0(CODE_HYDRO,"_Qec"),
  #  beginDateTime = as.POSIXct(DATE_H_MODEL[1], format = "%Y-%m-%d %H:%M:%S"),
  #  endDateTime = as.POSIXct(DATE_H_MODEL[length(DATE_H_MODEL)], format = "%Y-%m-%d %H:%M:%S")
  #)$observationDateTime
  
  # on met au pas de temps horaire fixe les debits a la station : Apremont, vouraie, mervent, rochereau
  # DEBIT_STATION_PAS_TEMPS_FIXE = rep(NA, length(DATE_H_MODEL)) 
  
  #for (i in 1:length(DATE_H_MODEL)) {
  #  
  #  m_date = which(substr(DATE_H_MODEL[i],1,16) == substr(DEBIT_STATION_DATE,1,16))
  #  
  #  if (length(m_date) != 0) DEBIT_STATION_PAS_TEMPS_FIXE[i] = DEBIT_STATION[m_date]
  
  #}
  
  
  ################################ 12 - Ecriture des resultats  ##############################################################
  
  logInfo("12 - Ecriture des resultats")
  
  # On ecrit dans WIMES les debits modelises 
  date_prevision_Q = format(Sys.time(),"%Y-%m-%d %H:%M:%S" ) 
  
  ############## on met les na en null 
  
  
  # on sauvegarde les niveaux dans les reservoirs de transfert   
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_RANDOM_FOREST_",CODE_HYDRO,"_R0"), 
      observationDateTime = DATE_H_MODEL,
      value = R_OUT[,2]
    )
  )
  
  # on sauvegarde les niveaux dans les reservoirs de transfert   
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_RANDOM_FOREST_",CODE_HYDRO,"_S0"), 
      observationDateTime = DATE_H_MODEL,
      value = S_OUT[,2]
    )
  )
  
  
  # on sauvegarde les debits issus du modele hydrologique
  upsertQuantValues(
    data.frame(
      timeSeriesCode =paste0("BRL_GR5J_RANDOM_FOREST_",CODE_HYDRO,"_Q"),
      observationDateTime =  DATE_H_MODEL,
      value = DEBIT_ENTREE_2
    )
  )   
  
}


# TODO renommer fonction, ajouter en-tete fonction
myFun_GR = function(InputsModel, RunOptions, PRODUCTION, ECHANGE,
                    TRANSFERT,  HU , RunModel_GR5J) {
  return(RunModel(InputsModel = InputsModel, 
                  RunOptions = RunOptions,
                  Param = c(PRODUCTION, 
                            ECHANGE, 
                            TRANSFERT, 
                            HU), 
                  FUN = RunModel_GR5J))
}

# TODO renommer fonction, ajouter en-tete fonction






INITIALISATION_VARIABLES_PDT_FIXE = function( DATES,
                                              VARIABLE)
{
  VARIABLE_TEMP = rep(NA,length(DATES))
  for ( i in 1:length(DATES))
  {
    M_DATE = which(DATES[i] ==VARIABLE$observationDateTime)
    if(length(M_DATE)!=0 )   VARIABLE_TEMP[i] = VARIABLE$value[ M_DATE]     
  } 
  return(data.frame(DATES,VARIABLE_TEMP))
}

    



OPTIMISATION_RESERVOIR_TRANFERT=function(parametres_station = parametres_station,
                                         DATE_H_MODEL = DATE_H_MODEL,
                                         PLUIE_DATE_HOURS = PLUIE_DATE_HOURS , 
                                         ETP_DATE_HOUR = ETP_DATE_HOUR , 
                                         S0 = S0,
                                         Q0 = Q0,
                                         DATE_RECALAGE = DATE_RECALAGE){
  
  # on fait passer la totalite des niveaux d eau initiale dans le reservoir de transfert
  R0_SIM = seq(1, (parametres_barrage$TRANSFERT - 1), 0.1)
  
  # vecteur qui recoit les resultats 
  Q0_SIM = rep(0, length(R0_SIM))
  
  # lacement de la boucle sur les niveaux initiaux     
  for (i_R in seq(along = R0_SIM)) {
   
    #print(i_R)
    # preparation of InputsModel object                   
    # attention on met forcement la pluie nulle au premier pas de temps pour connaitre les etats initiaux 
    OutputsModel = tryCatch(
      myFun_GR_2(S0, R0_SIM[i_R],parametres_station,DATE_H_MODEL,PLUIE_DATE_HOURS,ETP_DATE_HOUR),
      error = function(e) {
        # TODO JWI
        logError(paste(e$message, i_R))
        logError("Pas d'optimisation pour ce niveau de réservoir")
        OutputsModel = list()
        OutputsModel$DatesR = DATE_RECALAGE
        OutputsModel$Qsim = rep(0,length(DATE_RECALAGE))
        return(OutputsModel)
      }
    )
    
  # On recupere la lame d eau de la modelisation + conversion des mm/h en m3/s
  Q0_SIM[i_R] = as.numeric(OutputsModel$Qsim[ which( OutputsModel$DatesR == DATE_RECALAGE ) ] *parametres_station$SUPERFICIE / (3.6))
    
    
  }
  
  # on choist le m_b qui presente le plus faible ecart  entre le d?bit observ? Q0 et le d?bit simul?
  m_b = which.min(abs(as.numeric(Q0) - Q0_SIM) / as.numeric(Q0))
  
  # determination du R0 
  R0 = R0_SIM[m_b] /parametres_station$TRANSFERT
  
  # sortie du R0
  return(R0)
  
}


GetGeometry = function(bvCode) {
  tryCatch(
    {
      logInfo(paste("Getting the geometry of the site ", bvCode, "...", sep = ""))
      
      Rgeom = tryCatch(
        getSite(bvCode),
        error = function(e){
          stop(paste("Error during getSite request : ", e$message))
        }
      )
      
      if (Rgeom$geometry$type != "MultiPolygon") {
        stop(paste("The geometry type of", bvCode, "must be MultiPolygon."))
      }
      
      Rgeom = Rgeom$geometry$coordinates
      nb = length(Rgeom[,,,1])
      coord = lapply(1:nb, FUN = function(x) Rgeom[,,x,])
      
      # Creates the JSON object
      logInfo("Creating the JSON object...")
      init = TO_GeoJson$new()
      json = init$MultiPolygon(list(list(coord)), stringify = TRUE)
      
      # Reads JSON file as GDAL geometry
      logInfo("Reading JSON file as GDAL geometry...")
      bv = readOGR(json$json_dump, "OGRGeoJSON", verbose = FALSE)
      
      return(bv)
    },
    error = function(e){
      stop(paste("Error during getting the geometry of the site : ", e$message))
    }
  )
}




